package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
                this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public ShoppingCartPage getCartPage() {
        return new ShoppingCartPage(driver);
    }

    public SearchResultsPage getSearchPage() {
        return new SearchResultsPage(driver);
    }

    public iPhonePage getIPhonePage() {
        return new iPhonePage(driver);
    }

    public LaptopPage getLaptopPage() {
        return new LaptopPage(driver);
    }

    public SignInPage getSignInPage() {
        return new SignInPage(driver);
    }
}
