package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchField;

    @FindBy(xpath = "//input[@id='nav-search-submit-button']")
    private WebElement searchButton;

    @FindBy(xpath = "//span[@class='nav-line-2 ']")
    private WebElement signInButton;

    @FindBy(xpath = "//span[@class='icp-color-base'][contains(.,'English')]")
    private WebElement languageButton;

    @FindBy(xpath = "//span[contains(.,'Hello, Andrey')]")
    private WebElement WelcomingMessageOnTheMainPage;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public void isSearchFieldVisible() {
        searchField.isDisplayed();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void signIn() {
        signInButton.click();
    }

    public String getLanguageButtonText() {
        return languageButton.getText();
    }

    public boolean isThereAWelcomingMessageOnTheMainPage() {
        return WelcomingMessageOnTheMainPage.isDisplayed();
    }
}
