package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultsPage extends BasePage {

    @FindBy(xpath = "//span[contains(@class,'a-text-normal')][contains(text(),'Apple iPhone X, 64GB')]")
    private WebElement iPhoneButton;

    @FindBy(xpath = "//span[contains(text(),'Apple iPhone 11 Pro Max')]")
    private WebElement pickIphone;

    @FindBy(xpath = "//span[@class='a-size-medium a-color-base a-text-normal']" +
            "[contains(.,'Apple iPhone 11 Pro Max, 512GB, Midnight Green - Unlocked (Renewed Premium)')]")
    private WebElement ProductButton;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public void openIphonePage() {
        iPhoneButton.click();
    }

    public void pickIphoneFromList() {
        pickIphone.click();
    }

    public void clickOnAProduct() {
        ProductButton.click();
    }
}
