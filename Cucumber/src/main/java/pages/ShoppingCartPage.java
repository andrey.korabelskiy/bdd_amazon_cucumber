package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartPage extends BasePage {

    @FindBy(xpath = "//span[@id='sc-subtotal-label-activecart']")
    private WebElement getQuantity;

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchField;

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    public String getQuantityInCart() {
        return getQuantity.getText();
    }

    public void enterTextToSearchField(final String keyWord) {
        searchField.clear();
        searchField.sendKeys(keyWord, Keys.ENTER);
    }
}
