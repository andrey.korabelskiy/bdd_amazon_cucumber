package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//input[@id='ap_email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='ap_password']")
    private WebElement passwordField;

    @FindBy(xpath = "//h4[contains(.,'There was a problem')]")
    private WebElement PopUpThereWasAProblemWeCannotFindAnAccountWithThatEmailAddress;

    @FindBy(xpath = "//h4[contains(.,'There was a problem')]")
    private WebElement PopUpThereWasAProblemThePasswordIsIncorrect;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public void enterEmail(final String email) {
        emailField.sendKeys(email, Keys.ENTER);
    }

    public void enterPassword(final String password) {
        passwordField.sendKeys(password, Keys.ENTER);
    }

    public boolean isThereAPopUpThereWasAProblemWeCannotFindAnAccountWithThatEmailAddress() {
        return PopUpThereWasAProblemWeCannotFindAnAccountWithThatEmailAddress.isDisplayed();
    }

    public boolean isThereIsAPopUpThereWasAProblemThePasswordIsIncorrect() {
        return PopUpThereWasAProblemThePasswordIsIncorrect.isDisplayed();
    }
}
