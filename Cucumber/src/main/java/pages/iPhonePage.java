package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class iPhonePage extends BasePage {

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[@class='a-button-inner']/a[@id='hlb-view-cart-announce']")
    private WebElement goToCartButton;

    @FindBy(xpath = "//input[@id='buy-now-button']")
    private WebElement buyNowButton;

    @FindBy(xpath = "//span[@id='wishListMainButton']")
    private WebElement wishListIcons;

    @FindBy(xpath = "(//p[contains(.,'64GB')])[1]")
    private WebElement differentParameterButtonToChangeDefaultParameter;

    @FindBy(xpath = "//span[contains(.,'64GB')]")
    private WebElement appleIPhoneProMax64GBIsVisible;

    public iPhonePage(WebDriver driver) {
        super(driver);
    }

    public void addToTheCart() {
        addToCartButton.click();
    }

    public void goToCart() {
        goToCartButton.click();
    }

    public void clickBuyNowButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", buyNowButton);
    }

    public void clickWishListOnFirstProduct() {
        wishListIcons.click();
    }

    public void clicksOnDifferentParameterButtonToChangeDefaultParameter() {
        differentParameterButtonToChangeDefaultParameter.click();
    }

    public boolean getAppleIPhoneProMax64GBIsVisible() {
        return appleIPhoneProMax64GBIsVisible.isDisplayed();
    }
}