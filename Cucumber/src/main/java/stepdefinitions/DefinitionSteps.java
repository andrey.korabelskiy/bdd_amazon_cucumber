package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;
    WebDriver driver;
    HomePage homePage;
    PageFactoryManager pageFactoryManager;
    LaptopPage laptopPage;
    SignInPage signInPage;
    SearchResultsPage searchResultsPage;
    iPhonePage iphonePage;
    ShoppingCartPage shoppingCartPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks search field visibility")
    public void checkSearchVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isSearchFieldVisible();
    }

    @And("User check feature visibility")
    public void checkFeatureVisibility() {
        laptopPage = pageFactoryManager.getLaptopPage();
        laptopPage.checkFeatureVisibility();
    }

    @And("User makes search by keyword {string}")
    public void searchByKeyWord(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks search button")
    public void clickSearchButton() {
        homePage.clickSearchButton();
    }

    @And("User checks that url contains keyword {string}")
    public void checkUrlContainsKeyword(final String keyword) {
        assertTrue(driver.getCurrentUrl().contains(keyword));
    }

    @And("User click 'Featured' button")
    public void featuredButtonClick() {
        laptopPage.feature();
    }

    @And("User click sortByPrice button")
    public void sort() {
        laptopPage.sort();
    }

    @And("User check is sort was enabled")
    public void checkSort() {
        laptopPage.checkIfSortWasEnabled();
    }

    @And("User checks if goods was sorted correctly")
    public void checkPrice() {
        assertTrue(laptopPage.checkIfSortWasEnabled().contains("Price: Low to High"));
    }

    @And("User click signIn button")
    public void signIn() {
        homePage.signIn();
    }

    @And("User enters email {string}")
    public void emailField(final String email) throws InterruptedException {
        Thread.sleep(3000); //Bad practise, but if we don`t use sleep, CAPTCHA occurred
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.enterEmail(email);
    }

    @And("User enters password {string}")
    public void passwordField(final String password) throws InterruptedException {
        Thread.sleep(3000); //Bad practise, but if we don`t use sleep, CAPTCHA occurred
        signInPage.enterPassword(password);
    }

    @And("User clicks on the phone")
    public void clickOnThePhone() {
        searchResultsPage = pageFactoryManager.getSearchPage();
        searchResultsPage.openIphonePage();
    }

    @And("User click add to cart button")
    public void addToCart() {
        iphonePage = pageFactoryManager.getIPhonePage();
        iphonePage.addToTheCart();
    }

    @And("User goes to cart")
    public void goToCart() {
        iphonePage.goToCart();
    }

    @And("User check amount of products in cart {string}")
    public void checkAmountInCart(final String quantity) {
        shoppingCartPage = pageFactoryManager.getCartPage();
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(shoppingCartPage.getQuantityInCart().contains(quantity));
    }

    @And("User enters {string} to searchField")
    public void enterTextToSearchField(final String searchText) {
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        shoppingCartPage.enterTextToSearchField(searchText);
    }

    @And("User clicks on 'iPhone 11'")
    public void clickOnIphoneFromList() {
        searchResultsPage.pickIphoneFromList();
    }

    @And("User clicks on a product")
    public void userClicksOnAProduct() {
        searchResultsPage = pageFactoryManager.getSearchPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.clickOnAProduct();
    }

    @And("User clicks Buy now button on product")
    public void userClicksBuyNowButtonOnProduct() {
        iphonePage = pageFactoryManager.getIPhonePage();
        iphonePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        iphonePage.clickBuyNowButton();
    }

    @Then("User checks that page changed to {string}")
    public void userChecksThatPageChangedToExpectedChangedPage(final String expectedUrlResult) {
        assertTrue(driver.getCurrentUrl().contains(expectedUrlResult));
    }

    @And("User clicks wish list on first product")
    public void userClicksWishListOnFirstProduct() {
        iphonePage = pageFactoryManager.getIPhonePage();
        iphonePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        iphonePage.clickWishListOnFirstProduct();
    }

    @And("User checks that language switcher is {string}")
    public void checkLanguage(final String language) {
        assertTrue(homePage.getLanguageButtonText().equalsIgnoreCase(language));
    }

    @And("User clicks on 64GB button to change default parameter 512GB")
    public void userClicksOnDifferentParameterButtonToChangeDefaultParameter() {
        iphonePage = pageFactoryManager.getIPhonePage();
        iphonePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        iphonePage.clicksOnDifferentParameterButtonToChangeDefaultParameter();
    }

    @Then("Apple iPhone 11 Pro Max, 64GB is visible")
    public void appleIPhoneProMax64GBIsVisible() {
        iphonePage = pageFactoryManager.getIPhonePage();
        iphonePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(iphonePage.getAppleIPhoneProMax64GBIsVisible());
    }

    @Then("User checks if there is a pop up “There was a problem We cannot find an account with that email address”")
    public void userChecksIfThereIsAPopUpThereWasAProblemWeCannotFindAnAccountWithThatEmailAddress() {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(signInPage.isThereAPopUpThereWasAProblemWeCannotFindAnAccountWithThatEmailAddress());
    }

    @Then("User checks if there is a pop up “There was a problem The password is incorrect”")
    public void userChecksIfThereIsAPopUpThereWasAProblemThePasswordIsIncorrect() {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(signInPage.isThereIsAPopUpThereWasAProblemThePasswordIsIncorrect());
    }

    @Then("User checks the welcoming message on the main page")
    public void userChecksTheWelcomingMessageOnTheMainPage() {
        homePage = pageFactoryManager.getHomePage();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isThereAWelcomingMessageOnTheMainPage());
    }

    @After
    public void tearDown() {
        driver.close();
    }
}
