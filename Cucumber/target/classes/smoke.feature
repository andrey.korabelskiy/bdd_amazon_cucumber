Feature: Smoke tests for Amazon website
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Login with valid email and password
    Given User opens '<homePage>' page
    When User click signIn button
    And User enters email '<email>'
    And User enters password '<password>'
    Then User checks the welcoming message on the main page

    Examples:
      | homePage               | email                | password     |
      | https://www.amazon.com | andrey.nau@gmail.com | amazonqwerty |

  Scenario Outline: Login with invalid email
    Given User opens '<homePage>' page
    When User click signIn button
    And User enters email '<email>'
    Then User checks if there is a pop up “There was a problem We cannot find an account with that email address”

    Examples:
      | homePage               | email                     |
      | https://www.amazon.com | 562aasdf.adg44u@gmail.com |

  Scenario Outline: Login with valid email and invalid password
    Given User opens '<homePage>' page
    When User click signIn button
    And User enters email '<email>'
    And User enters password '<password>'
    Then User checks if there is a pop up “There was a problem The password is incorrect”

    Examples:
      | homePage               | email                | password |
      | https://www.amazon.com | andrey.nau@gmail.com | 1234asdf |

  Scenario Outline: Check url contains keyword
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    Then User checks that url contains keyword '<keyword>'

    Examples:
      | homePage               | keyword |
      | https://www.amazon.com | laptop  |

  Scenario Outline: Check sort product button
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User check feature visibility
    And User click 'Featured' button
    And User click sortByPrice button
    And User check is sort was enabled
    Then User checks if goods was sorted correctly

    Examples:
      | homePage               | keyword |
      | https://www.amazon.com | laptop  |

  Scenario Outline: Check add to cart
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on the phone
    And User click add to cart button
    And User goes to cart
    Then User check amount of products in cart '<quantity>'

    Examples:
      | homePage               | keyword | quantity |
      | https://www.amazon.com | iPhone  | 1        |

  Scenario Outline: Check that main page has language changer
    Given User opens '<homePage>' page
    When User checks search field visibility
    Then User checks that language switcher is '<languageSwitcher>'

    Examples:
      | homePage               | languageSwitcher |
      | https://www.amazon.com | English          |

  Scenario Outline: Check buy now button
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on a product
    And User clicks Buy now button on product
    Then User checks that page changed to '<expectedChangedPage>'

    Examples:
      | homePage               | keyword           | expectedChangedPage |
      | https://www.amazon.com | iphone 11 pro max | /ap/signin?         |

  Scenario Outline: Check parameters change on a Product page
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on a product
    And User clicks on 64GB button to change default parameter 512GB
    Then Apple iPhone 11 Pro Max, 64GB is visible

    Examples:
      | homePage               | keyword           |
      | https://www.amazon.com | iphone 11 pro max |

  Scenario Outline: Check add product to wishlist
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on a product
    And User clicks wish list on first product
    Then User checks that page changed to '<expectedChangedPage>'

    Examples:
      | homePage               | keyword           | expectedChangedPage |
      | https://www.amazon.com | iphone 11 pro max | /ap/signin?         |